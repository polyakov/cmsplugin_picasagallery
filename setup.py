import os
import sys
# hack for utf8 long_description support
reload(sys).setdefaultencoding("UTF-8")
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

version='1.0.0'

setup(
    name = 'cmsplugin_picasagallery',
    version = version,
    author = 'Pavel Polyakov',
    author_email = 'pollydrag@yandex.ru',
    url = 'http://bitbucket.org/ppolyakov/cmsplugin_picasagallery',
    download_url = 'http://bitbucket.org/ppolyakov/cmsplugin_picasagallery/get/tip.zip',

    description = 'Django CMS plugin for Picasagallery',
    long_description = open('README.rst').read().decode('utf8'),
    license = 'MIT license',
    requires = ['django_cms (>=2.1.2)','picasagallery',],

    packages=find_packages(),
    #package_data={'cmsplugin_picasagallery': []},

    classifiers=[
        'Development Status :: 4 - Beta',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Natural Language :: Russian',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    include_package_data=True,
    zip_safe = False,
    install_requires=['django-cms>=2.1.2','picasagallery'],
)
