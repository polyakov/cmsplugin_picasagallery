===================
cmsplugin_picasagallery
===================

cmsplugin_picasagallery - Плагин для Django CMS. App Hook для picasagallery

Лицензия MIT.

Зависимости
===========

* django-cms >= 2.1.2
* picasagallery

Установка
=========

Git::

    $ git clone https://ppolyakov@bitbucket.org/ppolyakov/cmsplugin_picasagallery.git
    $ cd cmsplugin_picasagallery
    $ python setup.py install


Добавить приложение в ``INSTALLED_APPS``::

    INSTALLED_APPS = (
        ...
        'cmsplugin_picasagallery',
        ...
    )

